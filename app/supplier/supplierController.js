(function(){
	
	angular
	.module('app.supplier')
	.controller('supplierController', [supplierController])

	function supplierController(){
		var vm = this;

		init();
		function init(){
			vm.suppliers = [
				{ name: "Supplier 1 Name", number: "+639-11-1111-111", location: "Mandaue" },
				{ name: "Supplier 2 Name", number: "+639-11-1111-111", location: "Cebu" },
				{ name: "Supplier 3 Name", number: "+639-11-1111-111", location: "Lapu-Lapu" },
				{ name: "Supplier 4 Name", number: "+639-11-1111-111", location: "Talisay" }
			]
		}
	}
})();