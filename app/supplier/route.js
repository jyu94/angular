(function(){
	'use strict';

	angular
    .module('app.supplier')
	.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('supplier', {
                url: "/supplier/",
                templateUrl: "supplier/index.html",
                controller: "supplierController",
                controllerAs: "vm"
            })
    }]);
})();