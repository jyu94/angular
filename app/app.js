(function(){
	'use strict';
	angular
	.module('app', [
		'ngMaterial',
		'ui.router',
		'md.data.table',
		'app.login',
		'app.inventory',
		'app.supplier',
		'app.rawmaterials'
	])
	.config(function($mdIconProvider, $mdThemingProvider){
		$mdThemingProvider.theme('default')
		.primaryPalette('pink')
		.accentPalette('red');
	});
})();