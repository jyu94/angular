(function(){
	
	angular
	.module('app.rawmaterials')
	.controller('rawMaterialsController', [rawMaterialsController])

	function rawMaterialsController(){
		var vm = this;

		init();
		vm.query = {
		    order: 'name',
		    limit: 5,
		    page: 1
		  };
		function init(){
			vm.rawmaterials = [
				{ name: "Peanut Butter", stock: "500 ml", status: "OK" },
				{ name: "Sugar", stock: "50 packs", status: "OK" },
				{ name: "Coffee Bean", stock: "4 packs", status: "Low on Stocks" },
				{ name: "Dough", stock: "5 packs", status: "Low on Stocks" },
				{ name: "Apple", stock: "4 pieces", status: "Low on Stocks" },
				{ name: "Banana", stock: "5 pieces", status: "Low on Stocks" }
			]
		}
	}
})();