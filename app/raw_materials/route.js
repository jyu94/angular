(function(){
	'use strict';

	angular
    .module('app.rawmaterials')
	.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('rawmaterials', {
                url: "/rawmaterials/",
                templateUrl: "raw_materials/index.html",
                controller: "rawMaterialsController",
                controllerAs: "vm"
            })
    }]);
})();