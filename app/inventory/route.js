(function(){
	'use strict';

	angular
    .module('app.inventory')
	.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('inventory-edit', {
                url: "/inventory/edit",
                templateUrl: "inventory/edit.html",
                controller: "inventoryEditController",
                controllerAs: "vm"
            })
    }]);
})();