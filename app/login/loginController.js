(function(){
	"use strict";

	angular
    .module('app.login')
    .controller("loginController", ['$state', '$stateParams', loginController])

    function loginController($state, $stateParams){
        var vm = this;
        vm.login = login;

        function login(){
            $state.go('home');
        }
    }

})()