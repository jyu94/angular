(function(){
	'use strict';

	angular
    .module('app.login')
	.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('login', {
                url: "/login",
                templateUrl: "login/index.html",
                controller: "loginController",
                controllerAs: "vm"
            })
    }]);
})();