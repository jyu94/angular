(function(){
	'use strict';
	angular
	.module('app')
	.controller('headerController', ['$scope', '$rootScope', '$mdMedia', '$mdSidenav', '$state', headerController]);

	function headerController($scope, $rootScope, $mdMedia, $mdSidenav, $state){
		var vm = this;

		init();
		function init() {
			vm.canOpen = false;
			vm.toggleSidenav = toggleSidenav;
			vm.openMenu = openMenu;
			vm.logout = logout;
			$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){
				checkState(toState);
			});
		}

		function checkState(state){
			vm.canOpen = state.name !== 'login';
		}

		function openMenu($mdMenu, ev) {
	      // originatorEv = ev;
	      $mdMenu.open(ev);
	    };

	    function logout() {
	    	$state.go('login');
	    }

		function toggleSidenav(component){
			console.log(component)
			$mdSidenav(component).toggle();
		}
	}
})();