(function(){
	'use strict';
	angular
	.module('app')
	.controller('sidebarController', ['$scope', '$rootScope', '$mdMedia', '$state', sidebarController]);

	function sidebarController($scope, $rootScope, $mdMedia, $state){
		var vm = this;
		
		init();
		function init() {
			vm.canOpen = false;
			$scope.state = $state;
			$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){
				checkState(toState);
			});

			vm.menu = [
				{
					title: "Inventory",
					icon: "assignment",
					link: "inventory-edit"
				},
				{
					title: "Purchases",
					icon: "attach_money",
				},
				{
					title: "Suppliers",
					icon: "local_offer",
					link: "supplier"
				},
				{
					title: "Raw Materials",
					icon: "assignment",
					link: "rawmaterials"
				}
			]

		}

		function checkState(state){
			vm.canOpen = state.name !== 'login';
		}
	}
})();