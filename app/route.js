(function () {
	'use strict';
    angular
    .module("app")
    .config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.hashPrefix('');
        $urlRouterProvider.otherwise("/");
        $stateProvider
            .state('home', {
                url: "/",
                template: "<div>Hello world</div>"
            })
    }]);
})();